import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue'
import {routes} from './routes';

Vue.use(VueRouter);
const router = new VueRouter({
  routes, //ES6 key and values same
  mode: 'history', //no #hastag tag
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    //if exist a hash on url, scroll to this selector
    if (to.hash) {
      return { selector: to.hash }
    }
    return {x: 0, y: 700};
  }
});
//before each router action finish
router.beforeEach((to, from, next) => {
  console.log('global beforeEach');
  //continue method
  next();
  //abort
  //next(false);
});

new Vue({
  el: '#app',
  router, //ES6 key value are the same
  render: h => h(App)
})
