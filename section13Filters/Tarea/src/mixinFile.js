export const mixinFile = {
    data() {
        return {
            countedStringMixin: '',
        }
    },
    computed: {
        countedComputedFromMixin() {
            let stringlength = this.countedStringMixin.length;
            return stringlength !== 0 ? `${this.countedStringMixin}  (${stringlength})` : '';
        }
    }
};