import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', function(value) {
    return value.toLowerCase();
});

Vue.mixin({
    created() {
        console.log('Global Mixin - Created Hook');
    }
});

Vue.filter('to-counted-string', function(value) {
    let stringlength = value.length;

    return stringlength !== 0 ? `${value}  (${stringlength})` : '';
});

new Vue({
  el: '#app',
  render: h => h(App)
})
