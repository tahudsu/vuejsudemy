import Vue from 'vue'
import App from './App.vue'

//create filter globally
Vue.filter('to-lowercase', function(value) {
    return value.toLowerCase();
});

//A global mixin is going to be added on every app component
Vue.mixin({
  created() {
    console.log('Global Mixin - created Hook')
  }
})
new Vue({
  el: '#app',
  render: h => h(App)
})
