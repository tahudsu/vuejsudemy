import Vue from 'vue'
import App from './App.vue'
//globally
//Vue.component('app-header', Header);
//Vue.component('app-body', Body);
//Vue.component('app-footer', Footer);

new Vue({
  el: '#app',
  render: h => h(App)
})
