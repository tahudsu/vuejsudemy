import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App.vue'

//add a plugin
Vue.use(VueResource);
//set global options for http service
Vue.http.options.root = 'https://vuejs-http-1f4b6.firebaseio.com/';
//method to intercept the action and do something if u want
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  //now with put on firebase can overwrite the data loaded after post method loaded
  if (request.method == 'POST') {
    request.method = 'PUT';
  }
  //this overwrite response 
  next(response => {
    //overwrites json method, returning only response.body as messages
    response.json = () => {
      return {
        messages: response.body
      }
    };
  })
});

new Vue({
  el: '#app',
  render: h => h(App)
})
