import Vue from 'vue'
import App from './App.vue'
/* 
bind directive bind(el, binding, vnode)
iserted(el, binding, vnode) when element is inserted on dom
update(el, binding, vnode, oldVnode) when element is updated
componentUpdated, once component is updated
unbind, once directive ir removed
*/
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    //el.style.backgroundColor = 'green';
    //el.style.backgroundColor = binding.value;
    var delay = 0;
    if (binding.modifiers['delayed']) {
      delay = 3000;
    }
    setTimeout(()=> {
      if (binding.arg == 'background') {
        el.style.backgroundColor = binding.value;
      } else {
        el.style.color = binding.value;
      }
    }, delay);
    
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
